FROM ubuntu
MAINTAINER Jave Ewentry&lt;j.ewentry@yandex.ru&gt;
RUN export TERM=xterm && clear && echo “[Installer] Hi! Now we update and upgrade your machine!”
RUN apt-get update && apt-get upgrade -y
RUN echo "[Installer] Okay, when we must install packages for work. Just make coffee and have a relax..."
RUN apt-get install git python3 curl python3-setuptools -y
RUN echo "[Installer] All was installed! Clonning project..."
RUN git clone https://gitlab.com/jave_ewentry11/slumberger11.git && cd slumberger11
RUN echo "[Installer] We must install pip and module requests for work" && sleep 1
RUN python3 /slumberger11/get-pip.py && echo "PIP was installed"
RUN pip install requests
RUN echo "[Installer] Module was installed! Thank you for using my setup script."

ENTRYPOINT ["python3", "/slumberger11/weatherreport.py"]

